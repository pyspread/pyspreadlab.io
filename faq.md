---
layout: default
title: FAQ
---

# FAQ

## What kind of code can a cell contain?

Note: The following text applies for pyspread >1.99.0

A cell can contain arbitrary lines of Python code. However, the last non-empty line must contain a Python expression such as a list comprehension or a generator expression. However, the last line cannot contain Python statements such as

```python
for i in xrange(10): pass
```

Variables that are defined by the lines before the last line are treated locally, i.e. they are not accessible from outside the cell.

The expression in the last line may be assigned to one global variable. That means
```python
a = 1
```
is valid and creates a global variable `a`. However,
```python
a = b = 1
```
is invalid in the last line of code.

## What are the boundaries for the number of rows, columns and tables?

The number of rows is limited to 1 000 000, the number of columns to 100 000 and the number of tables to 100.

These values can be adjusted in the file settings.py. Note that pyspread may become unresponsive for large grids.

## How can I change the format, in which a number is displayed?

pyspread employs the `__str__` method to display objects in cells.
For changing output formatting just subclass your current output class in the macro editor.

```python
class Float2(float):
    def __str__(self):
        return "{} <MyUnit>".format(round(self.real, 2))
```

You can then simply put `Float2(2.3)` in a cell in order to see `2.3 <MyUnit>`.

**Note:** If you want to calculate currencies with pyspread, please do not use `float`. Instead, consider using the `decimal` class or a dedicated money representation class from pip such as `money`.

## How can I display LaTeX formulas?

Convert your LaTeX formula to svg and change the cell renderer to image renderer.
The following svg conversion code by Gerard Marull-Paretas can be found at `https://gist.github.com/gmarull`.

```
from io import BytesIO
import matplotlib.pyplot as plt


# matplotlib: force computer modern font set
plt.rc('mathtext', fontset='cm')


def tex2svg(formula, fontsize=12, dpi=300):
    """Render TeX formula to SVG.
    Args:
        formula (str): TeX formula.
        fontsize (int, optional): Font size.
        dpi (int, optional): DPI.
    Returns:
        str: SVG render.
    """

    fig = plt.figure(figsize=(0.01, 0.01))
    fig.text(0, 0, r'${}$'.format(formula), fontsize=fontsize)

    output = BytesIO()
    fig.savefig(output, dpi=dpi, transparent=True, format='svg',
                bbox_inches='tight', pad_inches=0.0, frameon=False)
    plt.close(fig)

    output.seek(0)
    return output.read()
```

This approach may also be used when using sympy in pyspread. Simply convert the sympy objects with the `latex` function.
