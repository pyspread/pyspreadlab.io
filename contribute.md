---
layout: default
title: Contribute
navtitle: Contribute
---


![]({{ site.baseurl }} /images/contribute.svg) Contribute
======================

# How to Help

Thank you for your interest in improving *Pyspread*. *Pyspread* is
non-commercial. It lives from your contributions. Do not hesitate to help with
small things, e.g. stating a feature wish in the issue tracker.

For some concrete things that may help see below.

## Bug Reports

If you find any issues with pyspread, please file an issue in the [issue tracker](https://gitlab.com/pyspread/pyspread/-/issues).
Suggestions are also welcome.

## Gitter board
Read the [gitter board](https://gitter.im/pyspread/community) and answer questions.

## Patches

For patches / code contributions,

- fork the **development** branch and
- create a merge request that
- you refer to in an issue.

Please try solving only one issue or realize one feature per branch.


## High priority roadmap items

 * Help testing the pre-release [version 2.3-beta.1](https://gitlab.com/pyspread/downloads/-/raw/master/pre-releases/pyspread-2.3b1.tar.gz)([sig](https://gitlab.com/pyspread/downloads/-/raw/master/pre-releases/pyspread-2.3b1.tar.gz.sig)) that migrates pyspread to PyQt6.
   Correct font wights and line widths are of special interest because Qt6 changes definitions.
   
   One bug is of special interest: On a Manjaro KDE test system, the font weight for Arial is wrong, i.e. the font is bold even when it should be normal and normal when it should be bold. Other fonts (including Times New Roman) seem not to be affected. Confirmation of similar or differnt behavior would be appreciated. Please comment [Issue 116](https://gitlab.com/pyspread/pyspread/-/issues/116).

 * Help with current issues on the roadmap:
   - [Issue #109 : Open xlsx files](https://gitlab.com/pyspread/pyspread/-/issues/109)
     We believe that Excel file import that creates Python functions from Excel formulas (via pycel) may help future users.
     If you intend to contribute, please fork the `pycel` branch and submit a merge request.
