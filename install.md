---
layout: default
title: Install
navtitle: Install
meta_description:  >
  Download and install for Linux and Windows
---

# ![]({{ site.baseurl }} /images/install.svg) Download & Install

*pyspread* works on Linux, Posix platforms and Windows, on which
[PyQt6](https://www.riverbankcomputing.com/software/pyqt/intro){:target="blank"}
is available (pyspread <2.3 requires PyQt5). While there have been reports that *pyspread* can be used on
OS X, OS X is currently unsupported ([can you help?](contribute.html)).

## Install pyspread

It is recommended to install pyspread as a package that is provided for your operating system.
The table below shows for which operating systems, pyspread is available in which version.

![Packaged](https://repology.org/badge/vertical-allrepos/pyspread.svg?header&columns=4){:class="w3-card-4 w3-mobile"}

If pyspread is unavailable or outdated for your operating system, you can install it using one of the three methods below.

When using `pip`, a Python virtual environment (`venv`) is recommended. Some operating systems may nudge your towards this. `pipx` could be a solution if `venv` is no option for you. 

Furthermore, note that the QtSvg extensions for PyQT are required. For some operating systems, they are packaged separately from PyQt. Please make sure QtSvg is installed on your system before using `pip`.

### With pip

```bash
pip install pyspread
```

### From git

It is assumed that python3 and git are installed.

**Get sources and enter dir**
```bash
git clone https://gitlab.com/pyspread/pyspread.git
# or
git clone git@gitlab.com:pyspread/pyspread.git
# then
cd pyspread
```

**Install dependencies and pyspread**
```bash
pip3 install -r requirements.txt
# or if pip3 is not present
pip install -r requirements.txt
# next
python3 setup.py install
```

### From tarball

It is assumed that python3 and gpg are installed

**Download and verify tarball**
Download the latest [pyspread tarball](https://files.pythonhosted.org/packages/25/9a/7b311e3e180f2f6bc0cfcc9b4451b773c4205a0be080091aa3a6dcea11f6/pyspread-2.3.1.tar.gz) and its [gpg signature](https://gitlab.com/pyspread/downloads/-/raw/master/releases/pyspread-2.3.1.tar.gz.asc) into the same folder ([alternative download location](https://gitlab.com/pyspread/downloads/-/raw/master/releases/pyspread-2.3.1.tar.gz)).

```bash
gpg --verify pyspread-<version>.tar.gz.sig
```
Check the output for information if the signature is correct.

**Unpack tarball and enter dir**
```bash
tar xvzf pyspread-<version>.tar.gz
cd pyspread-<version>/
```

**Install dependencies and pyspread**
```bash
pip3 install -r requirements.txt
# or if pip3 is not present
pip install -r requirements.txt
# next
python3 setup.py install
```

**Launch pyspread**

```bash
$ pyspread
```

or after changing directory to the main folder

```bash
$ python -m pyspread
```

or

```bash
$ ./pyspread/pyspread.py 
```

## Outdated version (Python 2.x compatible)

The outdated version pyspread v1.1.3 is still available - [see install v1.x](install_v1.html).
Please use it only if you require Python 2.x compatibility.
Also note that it is incompatible with Python 3.
