#!/usr/bin/env python3


import json

def make_ln(rec):
    return "<url><loc>https://</loc></url>\n" % s


with open("_data/main_nav.json", "r") as f:
    mainNav = json.load(f)

with open("_data/manual_nav.json", "r") as f:
    manNav = json.load(f)


s = """<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
"""

for n in mainNav:
    if "target" in n:
        continue
    s += "\t<url><loc>https://pyspread.gitlab.io/%s</loc></url>\n" % n['url']

for n in manNav:
    s += "\t<url><loc>https://pyspread.gitlab.io/manual/%s</loc></url>\n" % n['url']

s += "</urlset>"

with open("sitemap.xml", "w") as f:
    f.write(s)
