---
layout: default
title: Welcome to pyspread
navtitle: Home
meta_description:  >
  pyspread is a non-traditional spreadsheet application that is based on 
  and written in the programming language Python. It expects Python 
  expressions in its grid cells, which makes a spreadsheet specific language 
  obsolete.

---

![]({{ site.baseurl }} /images/pyspread.png) Welcome to pyspread
===================


*pyspread* is a non-traditional spreadsheet application that is based on and written in the programming language Python.
The goal of *pyspread* is to be the most pythonic spreadsheet.

*pyspread* expects Python expressions in its grid cells, which makes a spreadsheet specific language obsolete. Each cell returns a
Python object that can be accessed from other cells. These objects can represent anything including lists or matrices.

*pyspread* is free software. It is released under the GPL v3 licence.

The latest release is [pyspread v2.3.1](https://pypi.org/project/pyspread/){:target="blank"} ([source code](https://gitlab.com/pyspread/pyspread){:target="blank"}). It requires Python 3.6+.

The last (outdated) release that is compatible with Python 2.x is pyspread v1.1.3.

![Pyspread example]({{ site.baseurl }} /images/screenshot_sinus_large.png){:class="w3-card-4 w3-mobile"}


## Features

* Cells accept Python code and return Python objects
* Access to Python modules from within cells including e.g. [NumPy](https://numpy.org/){:target="blank"}
* Imports CSV, SVG, XLSX**
* Exports CSV, SVG, PDF*
* Cells may display text, markup, images, dates** or charts**
* [Matplotlib](https://matplotlib.org/){:target="blank"} and [R](https://www.r-project.org/){:target="blank"} charts. [Plotnine](https://plotnine.readthedocs.io/en/stable/index.html){:target="blank"} and R packages [graphics](https://www.rdocumentation.org/packages/graphics){:target="blank"}, [lattice](https://lattice.r-forge.r-project.org/){:target="blank"}, [ggplot2](https://ggplot2.tidyverse.org/){:target="blank"} supported via dialog.**
* Spell checker**
* git-able pysu save file format
* blake2b based save file signatures that prevent foreign code execution

Notes: * PDF export via print to file, ** requires optional dependencies

## Target User Group

Directly using Python code in a grid is a core feature of *pyspread*. The target user group has
experience with or wants to learn the programming language Python:

* Clara is a research engineer. She systematically compares results of different parameter sets.
  She is proficient with Python and has used it for her scientific analyses. Clara wants a quick initial overview
  of how changing specific parameters affects her results. With *pyspread*, Clara is displaying each result in one cell.
  This allows her to quickly figure out, which parameters she is going to focus on next.

* Peter is using spreadsheets to prepare business decisions. 
  He separates data from code by keeping his business data in CSV files and accessing these files from *pyspread*. 
  Furthermore, Peter is worried about algorithmic errors and calculation inaccuracies in his complicated tables. 
  He is importing the *Money* datatype from [py-moneyed](https://github.com/py-moneyed/py-moneyed){:target="blank"}
  in the Macro panel. The dedicated Money class helps him avoiding floating point errors. 
  Peter has implemented his analysis tools together with unit tests in a separate Python module, 
  which he is importing as well. Therefore, he is worrying less about wrong results 
  that may lead to bad business decisions.

Not part of the target user group are Donna and Jack:

* Donna is looking for a free replacement for Ms. Excel. She does not know any programming language, and she has no time to learn Python.

* Jack does computation intensive data analysis. He is looking for a spreadsheet that can parallelize calculations - ideally out of the box as a cluster or a cloud solution.

This does not mean that Donna or Jack cannot work with *pyspread*. However, Donna might find the learning
curve for using Python code in cells too steep. Jack on the other hand might be disappointed because he has to 
ensure manually that his long running tasks are not locking up pyspread.

## Contact

For user questions or user feedback please use the
[pyspread community board](https://gitter.im/pyspread/community){:target="blank"}
on gitter.

For contributions, patches, development discussions and ideas please create an issue using the *pyspread* issue tracker
