---
layout: default
title: Documentation
navtitle: Docs
meta_description:  >
  Documentation central
---

![]({{ site.baseurl }} /images/docs.svg) Documentation
======================

## Tutorials
  - [Quick Start Tutorial](tutorial.html)
  - [Screencast](play_video.html) 
  
 
<h2 class="manual">User Manual</h2>
<ul>
<li><a href="manual/index.html">Index</a>
<ul>
{% for n in site.data.manual_nav %}
<li><a href="manual/{{n.url}}">{{n.title}}</a></li>
{% endfor %}
</ul>
</li>
</ul>
 
 
## Useful

  - [FAQ](/faq.html)
  - [Presentations with pyspread.pdf](https://pyspread.gitlab.io/media/pyspread_presentation/pyspread_presentation.pdf)
    ([.pys](https://pyspread.gitlab.io/media/pyspread_presentation/pyspread_presentation.pys) file used)
