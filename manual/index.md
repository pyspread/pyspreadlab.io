---
layout: default
section: manual
parent: ../
title: User Manual
---

This document provides a quick overview over *pyspread*. Readers should have at least some experience with [Python](https://www.python.org/).

<ul>
{% for n in site.data.manual_nav %}
<li><a href="{{n.url}}">{{n.title}}</a></li>
{% endfor %}
</ul>
